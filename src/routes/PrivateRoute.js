import React from 'react';
import { Redirect } from 'react-router-dom';

function PrivateRoute({ children }) {
  const user = localStorage.getItem('user')
    ? JSON.parse(localStorage.getItem('user'))
    : null;
  console.log(user);
  if (user) {
    return <Redirect to='/login' />;
  } else {
    return <div>{children}</div>;
  }
}

export default PrivateRoute;
