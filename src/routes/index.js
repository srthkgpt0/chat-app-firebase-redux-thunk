import React from 'react';
import { Switch } from 'react-router-dom';
import RouteWithLayout from './RouteWithLayout';
import HomePage from '../containers/HomePage';
import LoginPage from '../containers/LoginPage';
import RegisterPage from '../containers/RegisterPage';
import MainLayout from '../Layout/main';

function Routes() {
  return (
    <Switch>
      <RouteWithLayout
        exact
        component={HomePage}
        layout={MainLayout}
        path='/'
      />

      <RouteWithLayout
        exact
        component={LoginPage}
        layout={MainLayout}
        path='/login'
      />
      <RouteWithLayout
        exact
        component={RegisterPage}
        layout={MainLayout}
        path='/sign-up'
      />
    </Switch>
  );
}

export default Routes;
