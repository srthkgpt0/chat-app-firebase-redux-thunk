import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
function RouteWithLayout({ layout: Layout, component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={(matchProps) => (
        <Layout>
          <Component {...matchProps} />
        </Layout>
      )}
    />
  );
}
RouteWithLayout.propTypes = {
  layout: PropTypes.any.isRequired,
  component: PropTypes.any.isRequired,
};
export default RouteWithLayout;
// const user = localStorage.getItem('user')
//           ? localStorage.getItem('user')
//           : null;
//         if (user) {
//           return (
//             <Layout>
//               <Component {...matchProps} />
//             </Layout>
//           );
//         } else {
//           return <Redirect to='/login' />;
//         }
