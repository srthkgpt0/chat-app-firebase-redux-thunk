import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink, Link } from 'react-router-dom';
import { logout } from '../../actions/auth.actions';
import './styles.css';
function Header() {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  return (
    <header className='app__header'>
      <div className='header__content'>
        <div className='app__logo'>Web Messenger</div>
        {!auth.authenticated ? (
          <ul className='left__menu'>
            <li>
              <NavLink activeClassName='active' to='/login'>
                Login
              </NavLink>
            </li>
            <li>
              <NavLink activeClassName='active' to='/sign-up'>
                Sign Up
              </NavLink>
            </li>
          </ul>
        ) : null}
        <div className='userName'>
          {auth.authenticated ? `Hi ${auth.firstName} ${auth.lastName}` : null}
        </div>
        {auth.authenticated ? (
          <ul className='app__logout'>
            <li>
              <Link to='#' onClick={() => dispatch(logout(auth.uid))}>
                Logout
              </Link>
            </li>
          </ul>
        ) : null}
      </div>
    </header>
  );
}

export default Header;
