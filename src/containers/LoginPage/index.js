import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { signIn } from '../../actions/auth.actions';
import './styles.css';
function LoginPage() {
  const dispatch = useDispatch();
  const history = useHistory();
  const auth = useSelector((state) => state.auth);
  const [credentials, setCredentials] = useState({ email: '', password: '' });
  const onSubmit = (e, values) => {
    e.preventDefault();
    dispatch(signIn(credentials));
  };
  useEffect(() => {
    if (auth.authenticated) {
      history.push('/');
    }
  }, [auth]);

  return (
    <main className='main__content'>
      <h3>Login</h3>
      <form onSubmit={onSubmit}>
        <div className='content'>
          <div className='form__group'>
            <label htmlFor='email'>Email</label>
            <div className='input__group'>
              <input
                type='email'
                placeholder='Enter Email'
                id='email'
                value={credentials.email}
                onChange={(e) =>
                  setCredentials({ ...credentials, email: e.target.value })
                }
              />
            </div>
          </div>

          <div className='form__group'>
            <label htmlFor='password'>Password</label>
            <div className='input__group'>
              <input
                type='password'
                placeholder='Enter Password'
                id='password'
                value={credentials.password}
                onChange={(e) =>
                  setCredentials({ ...credentials, password: e.target.value })
                }
              />
            </div>
          </div>
          <div className='form__group'>
            <button type='submit' className='sign__in'>
              Sign In
            </button>
          </div>
        </div>
      </form>
    </main>
  );
}

export default LoginPage;
