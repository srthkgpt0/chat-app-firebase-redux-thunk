import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
// import { SIGNUP } from '../../redux/authSlice';
import { signup } from '../../actions/auth.actions';
import './styles.css';
function RegisterPage() {
  const dispatch = useDispatch();
  const history = useHistory();
  const auth = useSelector((state) => state.auth);
  const [credentials, setCredentials] = useState({
    email: '',
    password: '',
    firstName: '',
    lastName: '',
  });
  useEffect(() => {
    if (auth.authenticated) {
      history.push('/');
    }
  }, [auth]);
  const onSubmit = (e) => {
    e.preventDefault();
    console.log(credentials);
    dispatch(signup(credentials));
  };
  return (
    <main className='main__content'>
      <h3>Sign Up</h3>
      <form onSubmit={onSubmit}>
        <div className='content'>
          <div className='form__group'>
            <label htmlFor='firstName'>First Name</label>
            <div className='input__group'>
              <input
                type='text'
                placeholder='Enter First Name'
                id='firstName'
                value={credentials.firstName}
                onChange={(e) =>
                  setCredentials({ ...credentials, firstName: e.target.value })
                }
              />
            </div>
          </div>
          <div className='form__group'>
            <label htmlFor='lastName'>Last Name</label>
            <div className='input__group'>
              <input
                type='text'
                placeholder='Enter Last Name'
                id='lastName'
                value={credentials.lastName}
                onChange={(e) =>
                  setCredentials({ ...credentials, lastName: e.target.value })
                }
              />
            </div>
          </div>
          <div className='form__group'>
            <label htmlFor='email'>Email</label>
            <div className='input__group'>
              <input
                type='email'
                placeholder='Enter Email'
                id='email'
                value={credentials.email}
                onChange={(e) =>
                  setCredentials({ ...credentials, email: e.target.value })
                }
              />
            </div>
          </div>

          <div className='form__group'>
            <label htmlFor='password'>Password</label>
            <div className='input__group'>
              <input
                type='password'
                placeholder='Enter Password'
                id='password'
                value={credentials.password}
                onChange={(e) =>
                  setCredentials({ ...credentials, password: e.target.value })
                }
              />
            </div>
          </div>
          <div className='form__group'>
            <button type='submit' className='sign__in'>
              Sign Up
            </button>
          </div>
        </div>
      </form>
    </main>
  );
}

export default RegisterPage;
