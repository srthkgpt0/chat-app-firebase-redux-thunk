import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  getRealTimeMessages,
  getRealTimeUsers,
  updateMessage,
} from '../../actions';
import './styles.css';
import User from './User';
function HomePage() {
  const [chatStarted, setChatStarted] = useState(false);
  const [chatUser, setChatUser] = useState('');
  const [message, setMessage] = useState('');
  const [userUid, setUserUid] = useState(null);
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  const user = useSelector((state) => state.user);
  console.log(user);
  let unsubscribe;
  useEffect(() => {
    unsubscribe = dispatch(getRealTimeUsers(auth.uid))
      .then((unsubscribe) => {
        return unsubscribe;
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  useEffect(() => {
    return () => {
      unsubscribe.then((f) => f()).catch((error) => console.log(error));
    };
  }, []);

  const initChat = (user) => {
    setChatStarted(true);
    setChatUser(`${user.firstName} ${user.lastName}`);
    setUserUid(user.uid);
    dispatch(getRealTimeMessages({ uid_1: auth.uid, uid_2: user.uid }));
  };
  const submitMessage = (e) => {
    const msgObj = {
      user_oid_1: auth.uid,
      user_oid_2: userUid,
      message,
    };
    if (message !== '') {
      dispatch(updateMessage(msgObj)).then(() => {
        setMessage('');
      });
    }
    // console.log(msgObj);
  };
  return (
    <section className='container'>
      <div className='user__list'>
        {user.users.length > 0
          ? user.users.map((user) => (
              <User user={user} key={user.uid} onClick={initChat} />
            ))
          : null}
      </div>
      <div className='chatArea'>
        <div className='chatHeader'>{chatStarted ? chatUser : ''}</div>
        <div className='messageSection'>
          {chatStarted
            ? user.conversations.map((con, index) => (
                <div
                  key={index}
                  style={{
                    textAlign: con.user_oid_1 === auth.uid ? 'right' : 'left',
                  }}
                >
                  <p className='messageStyle'>{con.message}</p>
                </div>
              ))
            : null}
        </div>
        {chatStarted ? (
          <div className='chatControls'>
            <textarea
              value={message}
              onChange={(e) => setMessage(e.target.value)}
              placeholder='Write message!!'
            />
            <button onClick={submitMessage}>Send</button>
          </div>
        ) : null}
      </div>
    </section>
  );
}

export default HomePage;
