import React from 'react';

function User(props) {
  const { user, onClick } = props;
  return (
    <div onClick={() => onClick(user)} className='displayName'>
      <div className='displayPicture'>
        <img src='https://images.unsplash.com/photo-1495616811223-4d98c6e9c869?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=889&q=80' />
      </div>

      <span className='user__name'>
        {user.firstName} {user.lastName}
      </span>
      <span
        className={user.isOnline ? 'onlineStatus' : 'onlineStatus offLine'}
      ></span>
    </div>
  );
}

export default User;
