import { db } from '../firebase';
import { userConstants } from './constants';

export const getRealTimeUsers = (uid) => {
  return async (dispatch) => {
    dispatch({
      type: `${userConstants.GET_REALTIME_USERS}_REQUEST`,
    });
    const unsubscribe = db
      .collection('users')
      .where('uid', '!=', uid)
      .onSnapshot((querySnapshot) => {
        const users = [];
        querySnapshot.forEach(function (doc) {
          users.push(doc.data());
        });
        dispatch({
          type: `${userConstants.GET_REALTIME_USERS}_SUCCESS`,
          payload: { users },
        });
      });
    return unsubscribe;
  };
};

export const updateMessage = (msgObj) => {
  return async (dispatch) => {
    db.collection('conversations')
      .add({
        ...msgObj,
        isView: false,
        createdAt: new Date(),
      })
      .then((data) => {
        console.log(data);
        //success
        // dispatch({
        //   type:userConstants.GET_REALTIME_MESSAGES,

        // })
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const getRealTimeMessages = (user) => {
  return async (dispatch) => {
    db.collection('conversations')
      .where('user_oid_1', 'in', [user.uid_1, user.uid_2])
      .orderBy('createdAt', 'asc')
      .onSnapshot((querySnapshot) => {
        const conversations = [];
        querySnapshot.forEach((doc) => {
          if (
            (doc.data().user_oid_1 === user.uid_1 &&
              doc.data().user_oid_2 === user.uid_2) ||
            (doc.data().user_oid_1 === user.uid_2 &&
              doc.data().user_oid_2 === user.uid_1)
          ) {
            conversations.push(doc.data());
          }

          if (conversations.length > 0) {
            dispatch({
              type: userConstants.GET_REALTIME_MESSAGES,
              payload: { conversations },
            });
          } else {
            dispatch({
              type: `${userConstants.GET_REALTIME_MESSAGES}_FAILURE`,
              payload: { conversations },
            });
          }
        });
        console.log(conversations);
      });
  };
};
