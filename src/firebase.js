import firebase from 'firebase';
const firebaseConfig = {
  apiKey: 'AIzaSyCHSRzQ7gS4HBYzI7domWKsp9IDjsDdp0Q',
  authDomain: 'web-messenger-d5946.firebaseapp.com',
  projectId: 'web-messenger-d5946',
  storageBucket: 'web-messenger-d5946.appspot.com',
  messagingSenderId: '118599688470',
  appId: '1:118599688470:web:d03926d438ce31081eb037',
  measurementId: 'G-FZ8QR0BXMF',
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();
const auth = firebase.auth();

export { db, auth };
