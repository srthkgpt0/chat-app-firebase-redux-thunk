import React from 'react';
import Header from '../components/Header';

function LoginLayout({ children }) {
  return (
    <>
      <Header />
      {children}
    </>
  );
}

export default LoginLayout;
